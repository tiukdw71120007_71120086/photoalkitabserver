<?php
	$xml = simplexml_load_file('coba.xml');
	list($hasilSHA1, $asliSHA1) = hitungUlangSHA1 ($xml);

	if($hasilSHA1 == $asliSHA1){
		echo "Hasil SHA-1 sama <br />";
		$pubKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDLZ0cu8Cj+eCVntpz/jfW9gJvut2FUM+dtqsGt
b0E02fssBWZ0MwkaQbFGsARtun9MpWPoqCfh6W2XA5rzfaa0R8N0tTJAzv5UUgsvD4XKbn/9n269
2gwDJNzdlL9FPAjpG80GcbK6FQ2tbas+qP53JMC8W/jFSZBqcYVzq7hu6wIDAQAB
";		
		$key = getPublicKey($pubKey); 
		
        $signature = base64_decode(getSignatureValue($xml));
        

		$hasilVerifyDigitalSignature = openssl_verify(base64_decode($asliSHA1), $signature, $key);

		if($hasilVerifyDigitalSignature){
			echo "Signature berhasil di verifikasi";
		} else {
			echo "Signature gagal di verifikasi";
		}

	} else {
		echo "Hasil SHA-1 tidak sama";
	}

	function getPublicKey($pubkey){
		$pubkey = trim($pubkey);
		$key =  "-----BEGIN PUBLIC KEY-----\n".
        chunk_split($pubkey, 64,"\n").
       '-----END PUBLIC KEY-----';
        $key = openssl_get_publickey($key);
        return $key;
	}

	function hitungUlangSHA1 ($xml){
		$valueDigest = getDigestValue($xml);
		$valueImage = getImageValue($xml);
		//$valueSignature = getSignatureValue($xml);

		$trimText = trim($valueImage);
		$hasilHash = hash('sha1',$trimText);

		$trimText2 = trim($valueDigest);
		
		return array($hasilHash, $trimText2);
	}

	function getDigestValue ($xml) {
		$digest = $xml->xpath('//DigestValue');
		foreach ($digest as $digestt) {
			$valueDigest = $digestt;
		}
		return $valueDigest;
	}

	function getSignatureValue ($xml) {
		$signature = $xml->xpath('//SignatureValue');
		foreach ($signature as $signaturee) {
			$valueSignature = $signaturee;
		}
		return $valueSignature;
	}

	function getImageValue ($xml) {
		$image = $xml->xpath('//imageFile');
		foreach ($image as $imageFile) {
			$valueImage = $imageFile;
		}
		return $valueImage;
	}
?> 